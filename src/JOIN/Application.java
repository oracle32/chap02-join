package JOIN;

public class Application {
/*
	 * -- Join : 두개의 테이블을 합쳐서 결과를 조회해보자.
-- JOIN ( 오라클 전용구분 , ANSI 전용구분) 두가지로 나누어 진다.

SELECT
       E.EMP_ID
     , E.EMP_NAME
     , E.DEPT_CODE
     , D.DEPT_TITLE
  FROM EMPLOYEE E
  JOIN DEPARTMENT D ON (E.DEPT_CODE = D.DEPT_ID);
-- JOIN을 통해서 (E.DEPT_CODE = D.DEPT_ID) 연결해주어 값을 내주었다.

-- 두컬럼명이 같은 경우는? 테이블 별칭 사용   
SELECT
       E.EMP_ID
     , E.EMP_NAME
     , E.JOB_CODE    -- 여기있는 JOB_CODE는 다른 테이블에 있지만 컬럼명이 같다.
     , J.JOB_NAME    -- 그래서 앞쪽에 E, J 이렇게 각 테이블인걸 명시해주었다. 
  FROM EMPLOYEE E
  JOIN JOB J ON(E.JOB_CODE = J.JOB_CODE); 

-- 컬럼명이 같을때 USING 처리 방법,  
SELECT
       EMP_ID
     , EMP_NAME
     , JOB_NAME
     , JOB_CODE
  FROM EMPLOYEE 
  JOIN JOB USING(JOB_CODE);
  
-- 부서 테이블과 지역 테이블을 조인하여 테이블에 모든 데이터를 조회하세요 ,
-- LOCATION_ID = LOCAL_CODE 를 연결해여 2개의 테이블을 연결하였다.
--ANSI 표준
SELECT
       *
  FROM DEPARTMENT
  JOIN LOCATION ON (DEPARTMENT.LOCATION_ID = LOCAL_CODE);
-- 위 문제의 오라클 전용  
SELECT
       *
  FROM DEPARTMENT
     , LOCATION 
 WHERE DEPARTMENT.LOCATION_ID = LOCAL_CODE;  
  
  
-- 조인의 기본은 EQUAL JOIN이다. (EQU JOIN이라고도 한다. ) EQUAL = 같다 라는뜻,
-- fk(외래키) ---> pk(기본키)
-- 대상테이블 ---> 조인 

-- JOIN 의 기본은 INNER JOIN & EQU JOIN 이다,
-- OUTER JOIN : 두 테이블의 지정하는 컬럼값이 일치하지 않느 행도 조인에 포함을 시킴, 반드시 OUTER JOIN임을 명시
/*
1. LEFT OUTER JOIN
2. RIGHT OUTER JOIN
3. FULL JOUTER JOIN


-- 1. LEFT OUTER JOIN 
SELECT
       EMP_NAME
     , DEPT_TITLE
  FROM EMPLOYEE     -- 직원관리 기준으로 레프트 
  LEFT JOIN DEPARTMENT ON (DEPT_CODE = DEPT_ID);  
  
SELECT
       EMP_NAME
     , DEPT_TITLE
  FROM EMPLOYEE
     , DEPARTMENT
 WHERE DEPT_CODE = DEPT_ID(+);
 
-- 2. RIGHT OUTER JOIN 
--ANSI표준
SELECT
       EMP_NAME
     , DEPT_TITLE
  FROM EMPLOYEE     -- 부서코드 기준으로 RIGHT 
  RIGHT JOIN DEPARTMENT ON (DEPT_CODE = DEPT_ID);  
  
SELECT
       EMP_NAME
     , DEPT_TITLE
  FROM EMPLOYEE     
     , DEPARTMENT
 WHERE DEPT_CODE(+) = DEPT_ID;    
     
-- 3. FULL JOUTER JOIN
SELECT
       EMP_NAME
     , DEPT_TITLE
  FROM EMPLOYEE     
  FULL JOIN DEPARTMENT ON (DEPT_CODE = DEPT_ID); 
  -- FULL JOIN은 오라클 불가능!
  
  
  


-- SELF JOIN : 같은 테이블 을 조인하는경우 자기 자신과 조인을 맺는다.
SELECT
       E1.EMP_ID
     , E1.EMP_NAME 사원명
     , E1.DEPT_CODE
     , E1.MANAGER_ID
     , E2.EMP_NAME 관리자이름
  FROM EMPLOYEE E1
  JOIN EMPLOYEE E2 ON(E1.MANAGER_ID = E2.EMP_ID);
  
-- 다중 JOIN : N개의 테이블을 조회할 때 사용한다.  
-- ANSI
-- 조인의 순서가 중요하다.
SELECT
       EMP_ID
     , EMP_NAME
     , DEPT_CODE
     , DEPT_TITLE
     , LOCAL_NAME
  FROM EMPLOYEE
  JOIN DEPARTMENT ON(DEPT_CODE = DEPT_ID)
  JOIN LOCATION ON(LOCATION_ID = LOCAL_CODE);
-- 오라클 구문
-- 조인의 순서가 필요없다..
SELECT
       EMP_ID
     , EMP_NAME
     , DEPT_CODE
     , DEPT_TITLE
     , LOCAL_NAME
  FROM EMPLOYEE
     , DEPARTMENT 
     , LOCATION 
 WHERE LOCATION_ID = LOCAL_CODE   
   AND DEPT_CODE = DEPT_ID;
   
위 두문장은 다중( 2개) 이상을 나타낼때 오라클은 순서가 상관없고,
ANSI 구문은 순서가 중요하다는걸 알아야 한다.


문제) 직급이 대리이면서 아시아 지역에 근무하는 직원을 조회하자,
사번, 이름, 직급명,부서명,근무지역명, 급여를 조회 하자 
-- ANSI 표준

SELECT
       E.EMP_ID
     , E.EMP_NAME
     , D.DEPT_TITLE
     , J.JOB_NAME
     , N.NATIONAL_NAME
     , E.SALARY
  FROM EMPLOYEE E
  JOIN DEPARTMENT D ON(E.DEPT_CODE = D.DEPT_ID)
  JOIN JOB J ON (E.JOB_CODE = J.JOB_CODE)
  JOIN LOCATION L ON (D.LOCATION_ID = L.LOCAL_CODE)
  JOIN NATIONAL N ON (L.NATIONAL_CODE = N.NATIONAL_CODE)
 WHERE J.JOB_NAME = '대리'
   AND L.LOCAL_NAME LIKE 'AS%'; 
  
  
	 * 
	 * 
	 * */

}
